// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyBH5fL4GRXSTrJEVgSuYdyuQTuFysu6OGQ',
    authDomain: 'zengo-county-city.firebaseapp.com',
    databaseURL: 'https://zengo-county-city.firebaseio.com',
    projectId: 'zengo-county-city',
    storageBucket: 'zengo-county-city.appspot.com',
    messagingSenderId: '863041897264',
    appId: '1:863041897264:web:90180fe397da90d3'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
