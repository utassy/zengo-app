import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class CountyService {

  constructor(private firestore: AngularFirestore  ) { }

  getAllCounty() {
    return this.firestore.collection('County').snapshotChanges();
  }
}
