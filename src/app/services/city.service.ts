import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class CityService {

  constructor(private firestore: AngularFirestore) { }

  getCitiesByID(id: string) {
    return this.firestore.collection('City', ref => ref.where('county', '==', id)).snapshotChanges();
  }

  updateCityName(id: string, cityName: string) {
    return this.firestore.collection('City').ref.doc(id).update('name', cityName);
  }

  deleteCity(id: string) {
    return this.firestore.collection('City').doc(id).delete();

  }

  addCity(cityName: string, zip: string, countyID: string) {
    const randomNumber = Math.floor(Math.random() * 999) + 101;
    let docID = cityName[0].toUpperCase() + cityName[1].toUpperCase()  + cityName[cityName.length - 1].toUpperCase();
    docID = docID + '-' + countyID + '-' + randomNumber;
    this.firestore.collection('City').doc(docID).set({
      name: cityName,
      zipCode: zip,
      county: countyID
    });
  }
}
