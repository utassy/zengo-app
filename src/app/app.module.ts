import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { MainComponent } from './main/main.component';
import { CountyService } from './services/county.service';
import { CityService } from './services/city.service';
import { ListOfCitiesComponent } from './main/list-of-cities/list-of-cities.component';
import { AppMaterialModule } from './app.material.module';
import { BrowserModule } from '@angular/platform-browser';
import { HeaderComponent } from './header/header.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSnackBarModule, MatDialogModule } from '@angular/material';
import { WarningDialogComponent } from './main/list-of-cities/warning-dialog/warning-dialog.component';
import { AddCityComponent } from './main/add-city/add-city.component';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    ListOfCitiesComponent,
    HeaderComponent,
    WarningDialogComponent,
    AddCityComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase, 'zengo-county-city'),
    AngularFirestoreModule,
    AppMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatDialogModule
  ],
  providers: [
    CountyService,
    CityService
  ],
  entryComponents: [
    WarningDialogComponent,
    AddCityComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
