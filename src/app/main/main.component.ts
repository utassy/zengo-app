import { Component, OnInit, ViewChild } from '@angular/core';
import { CountyService } from '../services/county.service';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { County } from '../interfaces/county.interface';
import { ListOfCitiesComponent } from './list-of-cities/list-of-cities.component';
import { MatDialog } from '@angular/material';
import { AddCityComponent } from './add-city/add-city.component';
import { CityService } from '../services/city.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  myControl = new FormControl();
  filteredOptions: Observable<any[]>;
  countyArray = [];
  listOfCities = false;
  selectedCountyID = '';
  loaded = Promise.resolve(false);
  runs = 0;
  @ViewChild(ListOfCitiesComponent, { static: false }) child: ListOfCitiesComponent;


  constructor(
    private countyService: CountyService,
    private dialog: MatDialog,
    private cityService: CityService) { }


  openAddDialog() {
    const dialogRef = this.dialog.open(AddCityComponent, {
      data: this.countyArray
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res !== undefined) {
        this.cityService.addCity(res.name, res.zipCode, this.selectedCountyID);
      }
    });
  }


  private _filter(value: string): any[] {
    const filterValue = value.toLowerCase();
    return this.countyArray.filter(option => option.name.toLowerCase().includes(filterValue));
  }

  listCities(countyID: string) {
    if (this.runs === 0) {
      this.selectedCountyID = countyID;
    } else {
      this.child.getCities(countyID);
    }
    this.runs++;
    this.listOfCities = true;
  }
  getCounties() {
    this.countyService.getAllCounty().subscribe(data => {
      let county: County;
      data.forEach(element => {
        county = element.payload.doc.data() as County;
        county.id = element.payload.doc.id;
        this.countyArray.push(county);
      });
      this.loaded = Promise.resolve(true);
    });
  }

  ngOnInit() {
    this.getCounties();
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
  }

}
