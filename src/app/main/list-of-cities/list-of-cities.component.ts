import { Component, OnInit, Input } from '@angular/core';
import { CityService } from 'src/app/services/city.service';
import { City } from 'src/app/interfaces/city.interface';
import { MatSnackBar, MatDialog } from '@angular/material';
import { WarningDialogComponent } from './warning-dialog/warning-dialog.component';

@Component({
  selector: 'app-list-of-cities',
  templateUrl: './list-of-cities.component.html',
  styleUrls: ['./list-of-cities.component.css']
})
export class ListOfCitiesComponent implements OnInit {

  @Input() selectedCounty: string;
  cities: City[] = [];
  editable = [];
  disabled = [];
  loaded = Promise.resolve(false);
  selectedCityName: string;

  constructor(
    private cityService: CityService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog) { }

  delete(index: number) {
    const dialogRef = this.dialog.open(WarningDialogComponent, {
      data: this.cities[index]
    });
    dialogRef.afterClosed().subscribe(res => {
      const th = this;
      if (res) {
        this.cityService.deleteCity(this.cities[index].id).then(() => {
          this.openSnackBar('Sikeres törlés', undefined);
        })
          .catch(err => {
            this.openSnackBar('Sikertelen törlés', undefined);
          });
      }
    });
  }

  cancel(index: number) {
    this.editable[index] = false;
    let j = 0;
    this.disabled.forEach(element => {
      this.disabled[j].delete = false;
      this.disabled[j].edit = false;
      j++;
    });
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  save(id: string, index: number) {
    const th = this;
    this.cityService.updateCityName(id, this.selectedCityName).then(() => {
      this.openSnackBar('Sikeres mentés', undefined);
    })
      .catch(err => {
        this.openSnackBar('Sikertelen mentés', undefined);
      });
    this.cancel(index);
  }

  modifyCity(index: number) {
    let j = 0;
    this.editable[index] = true;
    this.cities.forEach(element => {
      if (j !== index) {
        this.disabled[j].delete = true;
        this.disabled[j].edit = true;
      }
      j++;
    });
    this.selectedCityName = this.cities[index].name;
  }
  getCities(id: string) {
    this.loaded = Promise.resolve(false);
    let city: City;
    this.cityService.getCitiesByID(id).subscribe(data => {
      this.cities = [];
      this.editable = [];
      this.disabled = [];
      data.forEach(element => {
        const obj = {
          delete: false,
          edit: false
        };
        city = element.payload.doc.data() as City;
        city.id = element.payload.doc.id;
        this.cities.push(city);
        this.editable.push(false);
        this.disabled.push(obj);
      });
      this.loaded = Promise.resolve(true);
    });
  }
  ngOnInit() {
    this.getCities(this.selectedCounty);
  }

}
