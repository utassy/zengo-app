import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { City } from 'src/app/interfaces/city.interface';

@Component({
  selector: 'app-warning-dialog',
  templateUrl: './warning-dialog.component.html',
  styleUrls: ['./warning-dialog.component.css']
})
export class WarningDialogComponent implements OnInit {
  selectedCity: City;

  constructor(
    public dialogRef: MatDialogRef<WarningDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data

  ) {
    this.selectedCity = data as City;
  }

  closeDialog(del: boolean) {
    this.dialogRef.close(del);
  }

  ngOnInit() {
  }

}
