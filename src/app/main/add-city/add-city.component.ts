import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { County } from 'src/app/interfaces/county.interface';

@Component({
  selector: 'app-add-city',
  templateUrl: './add-city.component.html',
  styleUrls: ['./add-city.component.css']
})
export class AddCityComponent implements OnInit {

  cityForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<AddCityComponent>) {
    this.cityForm = this.formBuilder.group({
      name: ['', Validators.required],
      zipCode: ['', [Validators.required, Validators.pattern('^[0-9]*$'), Validators.minLength(4)]]
    });
   }

   save() {
     this.dialogRef.close(this.cityForm.value);
   }

   close() {
    this.dialogRef.close();
   }

  ngOnInit() {
  }

}
