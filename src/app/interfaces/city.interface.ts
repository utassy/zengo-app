export interface City {
    id?: string;
    name: string;
    county: string;
    zipCode: string;
}

