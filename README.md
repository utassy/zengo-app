# ZengoApp

Az applikáció a megyék és az azokhoz tartozó városok megjelenítéséért felelős, valamint ezeknek a szerkesztését, hozzáadását és létrehozásukat teszi lehetővé. Alap állapotban csak egy mat-select input mező érthető el, ahol a megyét tudjuk kiválasztani. Amennyiben kiválasztottunk egy megyét, a hozzátartozó városok kilistázódnak és elérhetőek lesznek a töröl és szerkeszt ikonok, valamint az előbb említett mat-select mellett egy add button is elérhető. Hozzáadásnál a város nevére és irányítószámára van szükség, ezek megadása után amennyiben valid a form Mentés gomb használható és adatbázisban mentésre kerül a város. Minden város mellett elérhető a törlés és szerkeszt gomb. Ha törlésre kattintuk egy felugró ablak megjelenik, hogy valóban törölni szeretnénk-e a várost. Szerkeszt gombra kattintva megjelennek a mentés és mégsem gombok, valamint város neve egy input mezőbe kerül, ahol szerkeszthetővé válik. Ha mentésre kattintuk az input mező értéke kerül mentésre a város neveként, mégse gombra való kattintás esetén pedig alap helyzetbe kerül a város és a hozzá tartozó fuknciók is. 

## Futtatás

ng serve parancs futása után az applikáció elérhető lokálisan a http://localhost:4200 címen.
Firebase hostingot használva továbbá elérhető a https://zengo-county-city.firebaseapp.com címen.

## Készítette

Utasi Arnold, utasi.arnold@gmail.com